let router = require('express').Router()
let fire = require('./fire')
let bodyParser = require('body-parser')
let db = fire.firestore()
let matkulRef = db.collection('matkul');
let modulRef = db.collection('modul');
router.use(bodyParser.json())

//matkul collection
router.get('/matkul', (req, res)=>{

    let allData = []
    matkulRef.orderBy('waktu', 'desc').get()
    .then(snapshot => {
        snapshot.forEach((hasil)=>{
            allData.push(hasil.data())
        })
        console.log(allData)
        res.send(allData)
    }).catch((error)=>{
        console.log(error)
    })
})

router.get('/matkul/tingkat/:tingkat', (req, res) => {
    let allData = []
    let tingkat = req.params.tingkat;
    let semB = (tingkat*2);
    let semA = (semB-1);

    matkulRef.where('semester', '==', semA).get()
    .then(snapshot => {
        snapshot.forEach((hasil) => {
            allData.push(hasil.data())
        })
        console.log(allData)
    }).catch((error) => {
        console.log('Error ambil data:', error)
    })

    matkulRef.where('semester', '==', semB).get()
    .then(snapshot => {
        snapshot.forEach((hasil) => {
            allData.push(hasil.data())
        })
        console.log(allData)
        res.send(allData)
    }).catch((error) => {
        console.log('Error ambil data:', error)
    })

})

router.get('/matkul/sem/:sem', (req, res)=>{

    let allData = [];
    let sem = parseInt(req.params.sem);

    matkulRef.where('semester', '==', sem).get()
    .then(snapshot => {
        snapshot.forEach((hasil)=>{
            allData.push(hasil.data())
        })
        console.log(allData)
        res.send(allData)
    }).catch((error)=>{
        console.log(error)
    })
})

router.post('/matkul', (req, res)=>{
    db.settings({
        timestampsInSnapshots: true
    })
    matkulRef.add({
        matkul: req.body.matkul,
        semester: req.body.semester,
        subject: req.body.subject,
        waktu: new Date()
    })
    res.send({
        matkul: req.body.matkul,
        semester: req.body.semester,
        object: req.body.subject,
        waktu: new Date()
    })
})

router.post('/matkul/form', (req, res)=>{
    db.settings({
        timestampsInSnapshots: true
    })
    matkulRef.add({
        matkul: req.body.matkul,
        semester: parseInt(req.body.semester),
        subject: req.body.subject,
        waktu: new Date()
    })
    res.render('matkul', { 
        title: 'Input Mata Kuliah',
        message:'Sukses input mata kuliah', 
        matkul: req.body.matkul,
    });
    
})
//modul collection
router.get('/modul', (req, res)=>{
    db.settings({
        timestampsInSnapshots: true
    })
    var allData = []
    modulRef.orderBy('waktu', 'desc').get()
    .then(snapshot => {
        snapshot.forEach((hasil)=>{
            allData.push(hasil.data())
        })
        console.log(allData)
        res.send(allData)
    }).catch((error)=>{
        console.log(error)
    })
})

router.get('/modul/matkul/:subject', (req, res)=>{

    let subject = req.params.subject;
    let allData = [];
    modulRef.where('subject','==', subject).get()
    .then(snapshot => {
        snapshot.forEach((hasil) => {
            allData.push(hasil.data())
        })
        console.log(allData)
        res.send(allData)
    }).catch((error) => {
        console.log(error)
    })
})

router.get('/modul/limit/:limit', (req, res)=>{

    let allData = []
    let limit = parseInt(req.params.limit)
    
    modulRef.orderBy('waktu', 'desc').limit(limit).get()
    .then(snapshot => {
        snapshot.forEach((hasil)=>{
            allData.push(hasil.data())
        })
        console.log(allData)
        res.send(allData)
    }).catch((error)=>{
        console.log(error)
    })
})


router.post('/modul', (req, res)=>{
    modulRef.add({
        title: req.body.title,
        semester: req.body.semester,
        subject: req.body.subject,
        url     : req.body.url,
        waktu: new Date()
    })
    res.send({
       message:"Berhasil input data",
       data: {
        title: req.body.title,
        semester: req.body.semester,
        subject: req.body.subject,
        url     : req.body.url,
        waktu: new Date()
       }
    })
})

router.post('/modul/form', (req, res)=>{
    modulRef.add({
        title: req.body.title,
        semester: req.body.semester,
        subject: req.body.subject,
        url     : req.body.url,
        waktu: new Date()
    })
    res.render('modul', { 
        title: 'Unggah Tautan Modul',
        message:'Sukses unggah tautan', 
        modul: req.body.title,
    });
})

module.exports = router;