var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'It Works!'});
});

router.get('/matkul/form', function(req, res, next) {
  res.render('matkul',{title: 'Input Mata Kuliah'});
});

router.get('/modul/form', function(req, res, next) {
  res.render('modul',{title: 'Unggah Tautan Modul'});
});

module.exports = router;
