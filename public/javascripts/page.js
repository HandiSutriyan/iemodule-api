$('.matkulR').hide();
$('.loading').hide();
$('.helper').hide();

$(".semester").change(function(){
    let regX = /^[1-8]$/i;
    let validate = regX.test(this.value);
    console.log(validate);
    console.log(this.value);
    $('.matkulR').hide();
    $('.loading').fadeIn(500);
    $('helper').fadeIn(500);
    if(validate != false){
        getData(this.value);
    } else {
        alert("Data semester tidak sesuai!");
        this.value = "";
        $('.loading').hide();
    }
});

$('.success').click(function() {
    $('.success').hide();
})

$('.subCode').change(function() {
    let regX = /^[a-z0-9]{3,7}$/i ;
    let validate = regX.test(this.value);
    console.log(validate);
    if(validate != true){
        alert('Kode Harus seuai ketentuan')
        this.value="";
        $('helper').fadeIn(500);
    }
})

function getData(val) {
    $.get('/matkul/sem/'+val, function(data) {
        console.log('Data: ', data);
        $('.loading').hide();
        $('.matkulR').fadeIn(500);
        $('.matkul').contents().remove();
        $('.matkul').append("<option value='' selected disabled='disabled'>--pilih--</option>");
        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            console.log(data.length);
            $('.matkul').append("<option value="+element.subject+">"+element.matkul+"</option>");
        }
    })
}