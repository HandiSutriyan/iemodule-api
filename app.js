var createError = require('http-errors');
var express = require('express')
var bodyParser = require('body-parser')
var cors = require('cors')
const serverless = require('serverless-http');
var routeSaya = require('./routes/route')
var path = require('path')
const router = express.Router();

var indexRouter = require('./routes/index')

var app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/.netlify/functions/server', router);
app.use(routeSaya)

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
  });

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
module.exports = app;
module.exports.handler = serverless(app);